Zumpii

Language :- Dart
Front-end Framework :- Flutter
Back-end Framework :- Laravel
Scope :- This is a hybrid application (build on IOS and Android), which will facilitate user to book for any kind of services.
We have different types of roles which are defined as follows:-
a. Admin
b. Vendor
c. User
User End
User is covered under Application End of Zumpii. This section will cover all the users who want to avail the services.

At the time of sign-up, in addition to the basic information User has to select their preferred services and add address on the basis of Word, office and Home by selecting address on Google place finder.
Dashboard : Dashboard section will depict multiple services on the basis of its kind and services will display according to their distance.
Services are bifurcated on the basis of Hourly and Fixed. For eg: we have plumbing service whose sub-service is “leakage fix”, this comes under Fixed service and second we have “Car rental” service, which will be listed below hourly service.
User can select and book any service among the listed once, according to the availability of the vendor and near by user’s address location.
User can book the service now or he can schedule it later on the basis of Day and Time slots provided.
Coupon module: User will be provided with multiple different coupons to incur an advantage. Coupon will be of two types: Percentage based and fixed price coupon.
User can cancel it’s booked service, within a defined time frame.
Reschedule option is provided, to reschedule the booked service to other available time slot or date, However reschedule request has to be approved by the assigned Vendor.
Notification will be received regarding booked service, like Service started, in-progress, completed etc.

Vendor End
Another module of Application end is Vendor. Vendor is one person who provides services and is responsible to accept the booking request.

Sign up Module: Along with the basic information this module will cover the following:
a. Address
b. Upload valid documents to be used for authentication.
Dashboard module has 3 parts:
a. Booking requests, which has to be accepted by the vendor.
b. Upcoming bookings.
c. Completed bookings.
Vendor has the provision to cancel his assigned service.
Reschedule notification will be received corresponding to the services accepted by the Vendor.
Add schedule functionality is provided, according to the convenience of the Vendor.
Appointments can be viewed date wise.

This App will cover the following features which are common to both Vendor and User:

Chatting functionality is provided between Vendor and User in order to clear out any sort of doubt regarding service.
Facilitated with rating functionality, to be taken into consideration for future use.
Have the right to alter their own profile whenever needed.
Change Password feature is there, for security purpose.
Contact Us panel has been put up, in case any query arrived from user or vendor end.
User can manage, that is they can select, edit, modify, delete their address according as per requirement.
Referral code functionality: User can share their referral code with anyone and that person can register itself using referral code to get the exciting benefits.

Backend
Backend is handled by Admin, who is responsible for the below mentioned tasks:

Admin is responsible for the creation of category and then the services belongs to that category.
Date wise slots creation.
Admin has access to User list and their basic information.
Vendor list: It has all the information corresponding to any particular vendor  including, documents, status, availability etc.
Booking History: It will contain a list of all the bookings with their status, rating and all other details.
Coupon creation process is also handled by Admin.
Have rights to upload banners, which are visible on Application End.
Transaction list is displayed, having Admin commission and amount details.
Enquiry requests has to be answered by Admin.

Test credentails are as follow:
user :
Email - diksha904@yopmail.com
pwd - 123456789
vendor :
Email - shalini@yopmail.com
pwd - 12345678

Feel free to contact for further insights.
NOTE : Social login and online payments are under-progress.
